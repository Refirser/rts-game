﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using System;
using Newtonsoft.Json;

public class InventoryManager : MonoBehaviour
{
	#region 싱글톤
	private static InventoryManager instance;

	public static InventoryManager Instance {
		get { return instance; }
		set { instance = value; }
	}
	#endregion

	#region 아이템 정보
	private Dictionary<string, Dictionary<string, int>> itemInfo = new Dictionary<string, Dictionary<string, int>>();

	public Dictionary<string, Dictionary<string, int>> GetItemInfo {
		get { return itemInfo; }
	}

	private Dictionary<string, GameObject> itemInfoObj = new Dictionary<string, GameObject>();

	public Dictionary<string, GameObject> GetItemObj
	{
		get { return itemInfoObj; }
	}
	#endregion

	#region 인벤토리
	private List<ItemInstance> inv = new List<ItemInstance>();

	public List<ItemInstance> Inventory {
		get { return inv; }
		set { inv = value; }
	}

	public void TryInventoryInfo()
	{
		PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), LogSuccess, LogFailure);
	}

	private void LogSuccess(GetUserInventoryResult result)
	{
		Inventory = result.Inventory;
		UIManager.Instance.goldUI.text = result.VirtualCurrency["GD"].ToString();
		UIManager.Instance.cashUI.text = result.VirtualCurrency["CS"].ToString();
	}

	private void LogFailure(PlayFabError error)
	{
		
	}

	public void AddMoney(int amount)
	{
		var request = new AddUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = amount };
		PlayFabClientAPI.AddUserVirtualCurrency(request, (result) => Debug.Log("돈 추가 성공"), (error) => Debug.Log("돈 추가 실패"));
	}

	public void SubtractMoney(int amount)
	{
		var request = new SubtractUserVirtualCurrencyRequest() { VirtualCurrency = "GD", Amount = amount };
		PlayFabClientAPI.SubtractUserVirtualCurrency(request, (result) => Debug.Log("돈 빼기 성공"), (error) => Debug.Log("돈 빼기 실패"));
	}

	public void AddItem(string cat, string id)
	{
		string[] ids = new string[1];
		ids[0] = id;
		//var request = new PurchaseItemRequest() { CatalogVersion = "Parts", ItemId = id, VirtualCurrency = "GD", Price = 0 };
		PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest() { FunctionName = "GiveItems", FunctionParameter = new { Catalog = cat, ItemList = ids }, GeneratePlayStreamEvent = true }, onAddItem, onAddItemFailed);
		//PlayFabClientAPI.PurchaseItem(request, (result) => Debug.Log("구매 성공"), (error) => Debug.Log("구매 실패"));
	}

	private void onAddItem(ExecuteCloudScriptResult result) {
		Debug.Log("아이템 추가 완료");
		JsonObject jsonResult = (JsonObject)result.FunctionResult;
		jsonResult.TryGetValue("Data", out object data);
		jsonResult.TryGetValue("ID", out object id);

		if (itemInfo.ContainsKey(Convert.ToString(id))) {
			return;
		}

		Debug.Log(Convert.ToString(id));
		GameObject obj = Instantiate(Resources.Load(Convert.ToString(id)) as GameObject);
		itemInfoObj.Add(Convert.ToString(id), obj);

		Dictionary<string, string> jsondict = JsonConvert.DeserializeObject<Dictionary<string, string>>(Convert.ToString(data));
		Dictionary<string, int> dict = new Dictionary<string, int>();

		foreach (KeyValuePair<string, string> info in jsondict) {
			string[] keys = info.Key.Split('|');
			string[] values = info.Value.ToString().Split('|');

			if (keys[0] != null) {
				dict.Add(keys[0], Convert.ToInt32(values[0]));
				Debug.Log(keys[0] + " = " + values[0]);
			}

			if (keys[1] != null) {
				dict.Add(keys[1], Convert.ToInt32(values[1]));
				Debug.Log(keys[1] + " = " + values[1]);
			}
		}

		itemInfo.Add(Convert.ToString(id), dict);
	}

	private void onAddItemFailed(PlayFabError error)
	{


	}

	public void TakeItem(string id)
	{
		var request = new ConsumeItemRequest { ConsumeCount = 1, ItemInstanceId = id };
		PlayFabClientAPI.ConsumeItem(request, (result) => Debug.Log("사용 성공"), (error) => Debug.Log("사용 실패"));
	}

	public void BuyItem(string id, int money)
	{
		var request = new PurchaseItemRequest() { CatalogVersion = "Parts", ItemId = id, VirtualCurrency = "GD", Price = money };
		PlayFabClientAPI.PurchaseItem(request, (result) => Debug.Log("구매 성공"), (error) => Debug.Log("구매 실패"));
	}

	public void SellItem(string id, int money)
	{
		TakeItem(id);
		AddMoney(money);
	}
	#endregion

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else {
			Destroy(this.gameObject);
		}
	}

	private void Start()
	{
		
	}

	private void Update()
	{
		
	}

	/*#region 변수
	PlayfabManager playfabManager;

	public List<Item> inv = new List<Item>();
	public List<GameObject> objList = new List<GameObject>();
	private Dictionary<string, GameObject> itemList = new Dictionary<string, GameObject>();
	private static InventoryManager instance;
	#endregion

	private void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad(this);
		}

		playfabManager = PlayfabManager.Instance;

		for (int i = 0; i < objList.Count; i++)
		{
			GameObject obj = Instantiate(objList[i], this.gameObject.transform);

			Item item = obj.GetComponent<Item>();

			itemList.Add(item.ID, obj);
		}
	}

	private void Start()
	{

	}

	#region 싱글톤
	public static InventoryManager Instance {
		get { return instance; }
		set { instance = value; }
	}
	#endregion

	#region 데이터 저장 및 로드

	#endregion

	public void AddItem(string id) {
		inv.Add(itemList[id].GetComponent<Item>());
	}

	public void RemoveItem(int index) {
		inv.RemoveAt(index);
	}

	public bool HasItem(string id) {
		bool check = false;
		foreach (Item item in inv) {
			if (item.ID == id) {
				check = true;
				break;
			}
		}

		return check;
	}

	public int GetItemAmount(string id) {
		int amt = 0;

		foreach (Item item in inv)
		{
			if (item.ID != id)
			{
				continue;
			}
			amt = amt + 1;
		}

		return amt;
	}

	public void SetInventory(List<Item> list) {
		inv = list;
	}

	public List<Item> GetInventory() {
		return inv;
	}

	public void ResetInventory() {
		inv.RemoveRange(0,inv.Count);
	}*/
}
