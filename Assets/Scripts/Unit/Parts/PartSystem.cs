﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartSystem : MonoBehaviour
{
	private static PartSystem instance;

	public static BottomParts bottomParts = new BottomParts();
	public static MiddleParts middleParts = new MiddleParts();
	public static WeaponParts weaponParts = new WeaponParts();

	public static PartSystem Instance {
		get { return instance; }
	}

	private void Awake()
	{
		if (instance == null) {
			instance = this;
		}


	}

	private void Start()
	{
		bottomParts.AddData();
		bottomParts.LoadData();

		middleParts.AddData();
		middleParts.LoadData();

		weaponParts.AddData();
		weaponParts.LoadData();
	}
}
