﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private string id;
    private string name = "테스트용 하체 파츠";
    private int hp;
    private int armor;
    private int speed;
    private int watt;
    private int weight;
    private int damage;
    private int rpm;
    private int reach;
    private int partType;
    private int partSubType;

    private Vector3 offset;

    public string ID {
        get { return id; }
        set { id = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public int HP {
        get { return hp; }
        set { hp = value; }
    }

    public int Armor
    {
        get { return armor; }
        set { armor = value; }
    }

    public int Speed
    {
        get { return speed; }
        set { speed = value; }
    }

    public int Weight
    {
        get { return weight; }
        set { weight = value; }
    }

    public int Watt
    {
        get { return watt; }
        set { watt = value; }
    }

    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public int Rpm
    {
        get { return rpm; }
        set { rpm = value; }
    }

    public int Reach
    {
        get { return reach; }
        set { reach = value; }
    }

    public int PartType
    {
        get { return partType; }
        set { partType = value; }
    }

    public int PartSubType
    {
        get { return partSubType; }
        set { partSubType = value; }
    }

    public Vector3 Offset {
        get { return offset; }
        set { offset = value; }
    }
}
