﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PartEnum
{
	public enum Category { 
		Small,
		Medium,
		Large,
		Accessory,
		Blueprint,
	}

	public enum SubCategory { 
		Bottom,
		Middle,
		Weapon,
	}

	public enum PartType { 
		Small,
		Medium,
		Large,
		Accessory,
		Blueprint
	}
}
