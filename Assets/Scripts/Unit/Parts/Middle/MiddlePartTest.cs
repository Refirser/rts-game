﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class MiddlePartTest : Item
{

	private string id = "middlePartsTest";
	private string name = "테스트용 상체 파트";
	private Vector3 offset;

	private void Awake()
	{
		SetData();
	}

	private void SetData()
	{
		ID = id;
		Name = name;
		Offset = offset;
	}

	private void Start()
	{
		transform.position = Offset;
	}

}
