﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomPartTest : Item
{
	private string id = "bottomPartsTest";
	private string name = "테스트용 하체 파트";
	private Vector3 offset;

	private void Awake() {
		SetData();
	}

	private void SetData()
	{
		ID = id;
		Name = name;
		Offset = offset;
	}

	private void Start()
	{
		transform.position = Offset;
	}
}
