﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class BottomParts
{
	public List<string> partList = new List<string>();
	public Dictionary<string, int> healthList = new Dictionary<string, int>();
	public Dictionary<string, int> armorList = new Dictionary<string, int>();
	public Dictionary<string, int> speedList = new Dictionary<string, int>();
	public Dictionary<string, int> weightList = new Dictionary<string, int>();
	public Dictionary<string, int> wattList = new Dictionary<string, int>();
	public Dictionary<string, int> typeList = new Dictionary<string, int>();
	public Dictionary<string, int> subTypeList = new Dictionary<string, int>();

	public void AddData() {
		partList.Add("bottomPartsTest");
		healthList.Add("bottomPartsTest", 0);
		armorList.Add("bottomPartsTest", 0);
		speedList.Add("bottomPartsTest", 0);
		weightList.Add("bottomPartsTest", 0);
		wattList.Add("bottomPartsTest", 0);
		typeList.Add("bottomPartsTest", 0);
		subTypeList.Add("bottomPartsTest", 0);

		partList.Add("bottomPartsTest2");
		healthList.Add("bottomPartsTest2", 0);
		armorList.Add("bottomPartsTest2", 0);
		speedList.Add("bottomPartsTest2", 0);
		weightList.Add("bottomPartsTest2", 0);
		wattList.Add("bottomPartsTest2", 0);
		typeList.Add("bottomPartsTest2", 0);
		subTypeList.Add("bottomPartsTest2", 0);
	}

	public void LoadData()
	{
		//Debug.Log("Bottom Parts Data Load");
		for (int i =0;i< partList.Count;i++) {
			//Debug.Log(partList[i]);
			XmlNodeList nodes = XMLData.Instance.LoadXMLData("Data/Parts", partList[i]);

			if (nodes != null && nodes.Count > 0) {
				foreach (XmlNode node in nodes)
				{
					int data;
					System.Int32.TryParse(node.SelectSingleNode("health").InnerText, out data);
					healthList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("armor").InnerText, out data);
					armorList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("watt").InnerText, out data);
					wattList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("speed").InnerText, out data);
					speedList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("weight").InnerText, out data);
					weightList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("type").InnerText, out data);
					typeList[partList[i]] = data;

					System.Int32.TryParse(node.SelectSingleNode("subtype").InnerText, out data);
					subTypeList[partList[i]] = data;


					/*Debug.Log("name : " + partList[i]);
					Debug.Log("health : " + healthList[partList[i]]);
					Debug.Log("armor : " + armorList[partList[i]]);
					Debug.Log("watt : " + wattList[partList[i]]);
					Debug.Log("speed : " + speedList[partList[i]]);
					Debug.Log("weight : " + weightList[partList[i]]);
					Debug.Log("type : " + typeList[partList[i]]);
					Debug.Log("sub type : " + subTypeList[partList[i]]);*/
				}
			}

		}

	}
}
