﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Unit
{
    public class Unit : MonoBehaviour
    {
        #region 변수
        public GameObject owner;
        public GameObject target;
        public GameObject prefab;

        public int team;

        public float attackDistance = 100f;
        public float attackDelay = 5f;

        public float attackTimer;
        #endregion

        // Start is called before the first frame update
        void Awake()
        {

        }

        public void Start()
        {

        }

        #region 유닛 생성 및 파괴

        #endregion

        #region AI
        // Update is called once per frame
        public void Update()
        {
            if (target != null)
            {
                if (Vector3.Distance(target.transform.position, transform.position) <= attackDistance)
                {
                    if (attackTimer <= 0f)
                    {
                        Attack();
                        attackTimer += attackDelay;
                    }
                }
            }
        }

        public void LateUpdate()
        {

        }

        public void Attack()
        {

        }
        #endregion
    }
}
