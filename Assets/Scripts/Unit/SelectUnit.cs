﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace Unit
{
    public class SelectUnit : MonoBehaviour
    {
        #region 일반 변수
        public Player player;
        public Canvas canvas;
        public RawImage selectUI;
        public LayerMask layerMask;

        public bool isSelecting = false;

        Vector2 startPos;
        Vector2 endPos;

        public List<Unit> selectUnits = new List<Unit>();
        #endregion

        #region 싱글톤
        private static SelectUnit instance;
        #endregion

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {

                ToggleSelectionBox();
            }

            if (Input.GetMouseButton(0))
            {
                UpdateSelectionBox();
            }

            if (Input.GetMouseButtonUp(0))
            {
                ReleaseSelectionBox();

                if (selectUnits.Count == 0)
                {
                    CastRay();
                }
            }
        }

        public static SelectUnit Instance
        {
            get
            {
                return instance;
            }
        }

        #region 레이 캐스트

        void CastRay()
        {
            Vector3 pos = Input.mousePosition;
            pos.z = Camera.main.farClipPlane;

            Vector3 dir = Camera.main.ScreenToWorldPoint(pos);

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, dir, out hit, pos.z, layerMask))
            {
                hit.collider.transform.Find("Projector").gameObject.SetActive(true);
            }
        }
        #endregion

        #region 셀렉션 박스
        void ToggleSelectionBox()
        {
            isSelecting = true;
            startPos = Input.mousePosition;
            selectUI.gameObject.SetActive(true);

            foreach (Unit unit in selectUnits)
            {
                unit.GetComponent<Pathfinding.AIDestinationSetter>().target = null;
                unit.transform.Find("Projector").gameObject.SetActive(false);
            }
            selectUnits.Clear();

            selectUI.transform.localPosition = startPos;
        }

        void ReleaseSelectionBox()
        {
            isSelecting = false;
            selectUI.gameObject.SetActive(false);

            Vector2 min = selectUI.rectTransform.anchoredPosition - (selectUI.rectTransform.sizeDelta / 2);
            Vector2 max = selectUI.rectTransform.anchoredPosition + (selectUI.rectTransform.sizeDelta / 2);

            foreach (Unit unit in player.units)
            {
                Vector3 screenPos = Camera.main.WorldToScreenPoint(unit.transform.position);

                if (screenPos.x > min.x && screenPos.y > min.y && screenPos.x < max.x && screenPos.y < max.y)
                {
                    unit.transform.Find("Projector").gameObject.SetActive(true);
                    selectUnits.Add(unit);
                }

                selectUI.rectTransform.sizeDelta = new Vector2(0, 0);
            }


        }

        void UpdateSelectionBox()
        {
            endPos = Input.mousePosition;

            float width = endPos.x - startPos.x;
            float height = endPos.y - startPos.y;
            selectUI.rectTransform.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height));
            selectUI.rectTransform.anchoredPosition = startPos + new Vector2(width / 2, height / 2);
        }
        #endregion
    }
}
