﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using System;
using PlayFab.Json;

public class UnitInventory : MonoBehaviour
{
	public GameObject unitStatInfo;
	public GameObject partStatInfo;

	public GameObject scrollItem;
	public GameObject content;

	public GameObject categoryParent;
	public GameObject subCategoryParent;

	public Dictionary<PartEnum.Category, GameObject> categoryList = new Dictionary<PartEnum.Category, GameObject>();
	public Dictionary<PartEnum.SubCategory, GameObject> subCategoryList = new Dictionary<PartEnum.SubCategory, GameObject>();

	private int category;
	private int subCategory;

	#region 파트 오브젝트
	GameObject partobj;
	#endregion

	#region 파트 정보
	public GameObject unitName;
	public GameObject unitLevel;
	public GameObject unitWatt;
	public GameObject unitWeight;
	public GameObject unitDamage;
	public GameObject unitRpm;
	public GameObject unitSpeed;
	public GameObject unitReach;
	#endregion

	private void Awake()
	{
		if (unitStatInfo != null) {
			for (int i = 0; i < unitStatInfo.transform.childCount; i++) {
				unitStatInfo.transform.GetChild(i).gameObject.SetActive(false);
			}
			
		}

		if (partStatInfo != null)
		{
			for (int i = 0; i < partStatInfo.transform.childCount; i++)
			{
				partStatInfo.transform.GetChild(i).gameObject.SetActive(false);
			}
		}

		categoryList.Add(PartEnum.Category.Small, categoryParent.transform.Find("Small").gameObject);
		categoryList.Add(PartEnum.Category.Medium, categoryParent.transform.Find("Medium").gameObject);
		categoryList.Add(PartEnum.Category.Large, categoryParent.transform.Find("Large").gameObject);
		categoryList.Add(PartEnum.Category.Accessory, categoryParent.transform.Find("Accessory").gameObject);
		categoryList.Add(PartEnum.Category.Blueprint, categoryParent.transform.Find("Blueprint").gameObject);

		subCategoryList.Add(PartEnum.SubCategory.Bottom, subCategoryParent.transform.Find("BottomPart").gameObject);
		subCategoryList.Add(PartEnum.SubCategory.Middle, subCategoryParent.transform.Find("MiddlePart").gameObject);
		subCategoryList.Add(PartEnum.SubCategory.Weapon, subCategoryParent.transform.Find("WeaponPart").gameObject);

		for (int i = 0; i < categoryList.Count; i++) {
			int cat = i;
			categoryList[(PartEnum.Category)i].GetComponent<Button>().onClick.AddListener(() => onCategoryClick(cat));
		}

		for (int i = 0; i < subCategoryList.Count; i++)
		{
			int subcat = i;
			subCategoryList[(PartEnum.SubCategory)i].GetComponent<Button>().onClick.AddListener(() => onSubCategoryClick(subcat));
		}

		combineBtn.onClick.AddListener(() => onCombineClick());
	}

	#region 유닛 부품 클릭
	public void onCategoryClick(int num) {
		category = num;

		for (int i = 0; i < content.transform.childCount; i++) {
			Destroy(content.transform.GetChild(i).gameObject);
		}

		for(int i=0;i<InventoryManager.Instance.Inventory.Count;i++)
		{
			ItemInstance item = InventoryManager.Instance.Inventory[i];

			if (item == null) {
				continue;
			}

			int type = InventoryManager.Instance.GetItemInfo[item.ItemId]["Type"];
			int subtype = InventoryManager.Instance.GetItemInfo[item.ItemId]["SubType"];
			int hp = InventoryManager.Instance.GetItemInfo[item.ItemId]["HP"];
			int armor = InventoryManager.Instance.GetItemInfo[item.ItemId]["Armor"];
			int watt = InventoryManager.Instance.GetItemInfo[item.ItemId]["Watt"];
			int damage = InventoryManager.Instance.GetItemInfo[item.ItemId]["Damage"];
			int speed = InventoryManager.Instance.GetItemInfo[item.ItemId]["Speed"];
			int weight = InventoryManager.Instance.GetItemInfo[item.ItemId]["Weight"];
			int ats = InventoryManager.Instance.GetItemInfo[item.ItemId]["ATS"];
			int reach = InventoryManager.Instance.GetItemInfo[item.ItemId]["Reach"];


			if (type == category && subtype == subCategory) {
				GameObject info = Instantiate(scrollItem, content.transform);
				info.transform.Find("Text").GetComponent<Text>().text = item.DisplayName;
				info.GetComponent<Button>().onClick.AddListener(() => onClick(item.ItemId, item.DisplayName, hp, armor, watt, damage, speed, weight, ats, reach));
			}
		}
	}

	public void onSubCategoryClick(int num)
	{
		subCategory = num;

		for (int i = 0; i < content.transform.childCount; i++)
		{
			Destroy(content.transform.GetChild(i).gameObject);
		}

		for (int i = 0; i < InventoryManager.Instance.Inventory.Count; i++)
		{
			ItemInstance item = InventoryManager.Instance.Inventory[i];

			if (item == null)
			{
				continue;
			}

			int type = InventoryManager.Instance.GetItemInfo[item.ItemId]["Type"];
			int subtype = InventoryManager.Instance.GetItemInfo[item.ItemId]["SubType"];
			int hp = InventoryManager.Instance.GetItemInfo[item.ItemId]["HP"];
			int armor = InventoryManager.Instance.GetItemInfo[item.ItemId]["Armor"];
			int watt = InventoryManager.Instance.GetItemInfo[item.ItemId]["Watt"];
			int damage = InventoryManager.Instance.GetItemInfo[item.ItemId]["Damage"];
			int speed = InventoryManager.Instance.GetItemInfo[item.ItemId]["Speed"];
			int weight = InventoryManager.Instance.GetItemInfo[item.ItemId]["Weight"];
			int ats = InventoryManager.Instance.GetItemInfo[item.ItemId]["ATS"];
			int reach = InventoryManager.Instance.GetItemInfo[item.ItemId]["Reach"];


			if (type == category && subtype == subCategory)
			{
				GameObject info = Instantiate(scrollItem, content.transform);
				info.transform.Find("Text").GetComponent<Text>().text = item.DisplayName;
				info.GetComponent<Button>().onClick.AddListener(() => onClick(item.ItemId, item.DisplayName, hp, armor, watt, damage, speed, weight, ats, reach));
			}
		}
	}

	public void onClick(string id, string name, int hp, int armor, int watt, int damage, int speed, int weight, int ats, int reach) {
		if (partobj != null) {
			Destroy(partobj);
		}

		if (InventoryManager.Instance.GetItemObj[id] != null) {
			partobj = Instantiate(InventoryManager.Instance.GetItemObj[id], GameObject.Find("Part").transform);
			partobj.gameObject.SetActive(true);
			partobj.GetComponent<Item>().PartType = InventoryManager.Instance.GetItemInfo[id]["Type"];
			partobj.GetComponent<Item>().PartSubType = InventoryManager.Instance.GetItemInfo[id]["SubType"];
			Vector3 pos = partobj.transform.parent.position + (partobj.transform.parent.forward * 5f) + partobj.GetComponent<Item>().Offset;
			StartCoroutine(SetPartPos(partobj, pos));

			for (int i = 0; i < partStatInfo.transform.childCount; i++)
			{
				partStatInfo.transform.GetChild(i).gameObject.SetActive(true);
			}

			unitName.GetComponent<Text>().text = name;
			unitWatt.GetComponent<Slider>().value = watt;
			unitWeight.GetComponent<Slider>().value = weight;
			unitDamage.GetComponent<Slider>().value = damage;
			unitRpm.GetComponent<Slider>().value = ats;
			unitSpeed.GetComponent<Slider>().value = speed;
			unitReach.GetComponent<Slider>().value = reach;
		}
	}

	IEnumerator SetPartPos(GameObject obj, Vector3 pos) {
		yield return new WaitForSeconds(0.1F);
		if (obj != null) {
			obj.transform.position = pos;
		}
	}
	#endregion

	#region 유닛 조립
	public Button combineBtn;
	public Text combineWarning;

	public GameObject unitObj;
	
	private GameObject bottomPart;
	private GameObject middlePart;
	private GameObject weaponPart;

	private int bottomPartType;

	public void onCombineClick() {
		Debug.Log("Combine Click");
		if (partobj == null) return;
		
		Item comp;
		if(partobj.TryGetComponent<Item>(out comp))
		{
			combineWarning.text = "";
			if (comp.PartType == Convert.ToInt32(PartEnum.Category.Accessory) || comp.PartType == Convert.ToInt32(PartEnum.Category.Blueprint)) {
				return;
			}

			if (comp.PartSubType == Convert.ToInt32(PartEnum.SubCategory.Bottom))
			{
				
				if (bottomPart != null)
				{
					Destroy(bottomPart);
				}
				bottomPart = Instantiate(partobj, unitObj.transform);
				Vector3 pos = bottomPart.transform.parent.position + (bottomPart.transform.parent.forward * 5f) + bottomPart.GetComponent<Item>().Offset;
				StartCoroutine(SetPartPos(bottomPart, pos));
			}
			else if (comp.PartSubType == Convert.ToInt32(PartEnum.SubCategory.Middle))
			{
				if (bottomPart == null) {
					combineWarning.text = "하체 파트를 먼저 놓아주세요.";
					return;
				}
				if (middlePart != null)
				{
					Destroy(middlePart);
				}

				BoxCollider collider = bottomPart.GetComponent<BoxCollider>();

				middlePart = Instantiate(partobj, unitObj.transform);
				Vector3 pos = bottomPart.transform.position+ middlePart.GetComponent<Item>().Offset + new Vector3(0, collider.bounds.size.y, 0);
				StartCoroutine(SetPartPos(middlePart, pos));

				
			}
			else if (comp.PartSubType == Convert.ToInt32(PartEnum.SubCategory.Weapon))
			{
				if (bottomPart == null || middlePart == null)
				{
					combineWarning.text = "하체 파츠 및 접속 파츠를 먼저 놓아주세요.";
					return;
				}
				if (weaponPart != null)
				{
					Destroy(weaponPart);
				}

				BoxCollider collider = middlePart.GetComponent<BoxCollider>();

				weaponPart = Instantiate(partobj, unitObj.transform);
				Vector3 pos = middlePart.transform.position + weaponPart.GetComponent<Item>().Offset + new Vector3(0, collider.bounds.size.y, 0);
				StartCoroutine(SetPartPos(weaponPart, pos));
			}
		}
	}
	#endregion
}
