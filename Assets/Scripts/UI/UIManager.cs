﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	#region 싱글톤
	private static UIManager instance;
    
    public static UIManager Instance {
        get { return instance; }
        set { instance = value; }
    }
    #endregion

    #region 플레이어 정보
    public Text nicknameUI;
    public Text levelUI;
    public Text goldUI;
    public Text cashUI;
    
	#endregion

	#region 일반 함수
	private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(this.gameObject);
        }

        
    }

    // Start is called before the first frame update
    void Start()
    {
        //nicknameUI.text = PlayfabManager.Instance.Nickname;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	#endregion
}
