﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
	public Transform startPosition;
	public LayerMask wallMask;
	public Vector2 gridWorldSize;
	public float nodeRadius;
	public float distance;

	Node[,] grid;
	public List<Node> finalPath;

	float nodeDiameter;
	int gridSizeX, gridSizeY;

	private void Start()
	{
		nodeDiameter = nodeRadius;
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
		CreateGrid();
	}

	void CreateGrid() {
		grid = new Node[gridSizeX, gridSizeY];
		Vector3 bottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

		for (int y = 0; y < gridSizeX; y++) {
			for (int x = 0; x < gridSizeX; x++) {
				Vector3 worldPoint = bottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
				bool Wall = true;

				if (Physics.CheckSphere(worldPoint, nodeRadius, wallMask)) {
					Wall = false;
				}

				grid[y, x] = new Node(Wall, worldPoint, x, y);
			}
		}
	}

	public Node NodeFromWorldPosition(Vector3 worldPosition) {
		float xpoint = ((worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x);
		float ypoint = ((worldPosition.y + gridWorldSize.y / 2) / gridWorldSize.y);

		xpoint = Mathf.Clamp01(xpoint);
		ypoint = Mathf.Clamp01(ypoint);

		int x = Mathf.RoundToInt((gridSizeX - 1) * xpoint);
		int y = Mathf.RoundToInt((gridSizeY - 1) * ypoint);

		return grid[y, x];
	}

	public List<Node> GetNeighboringNodes(Node currentNode) {
		List<Node> neighboringNodes = new List<Node>();
		int xCheck;
		int yCheck;

		xCheck = currentNode.grid_x_Size + 1;
		yCheck = currentNode.grid_y_Size;

		if (xCheck >= 0 && xCheck < gridSizeX) {
			if (yCheck >= 0 && yCheck < gridSizeY) {
				neighboringNodes.Add(grid[xCheck, yCheck]);
			}
		}

		xCheck = currentNode.grid_x_Size - 1;
		yCheck = currentNode.grid_y_Size;

		if (xCheck >= 0 && xCheck < gridSizeX)
		{
			if (yCheck >= 0 && yCheck < gridSizeY)
				neighboringNodes.Add(grid[xCheck, yCheck]);
		}

		xCheck = currentNode.grid_x_Size;
		yCheck = currentNode.grid_y_Size + 1;

		if (xCheck >= 0 && xCheck < gridSizeX)
		{
			if (yCheck >= 0 && yCheck < gridSizeY)
				neighboringNodes.Add(grid[xCheck, yCheck]);
		}

		xCheck = currentNode.grid_x_Size;
		yCheck = currentNode.grid_y_Size - 1;

		if (xCheck >= 0 && xCheck < gridSizeX)
		{
			if (yCheck >= 0 && yCheck < gridSizeY)
				neighboringNodes.Add(grid[xCheck, yCheck]);
		}

		return neighboringNodes;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

		if (grid != null)
		{
			foreach (Node node in grid)
			{
				if (node.isWall)
				{
					Gizmos.color = Color.white;
				}
				else
				{
					Gizmos.color = Color.yellow;
				}

				if (finalPath != null)
				{
					Gizmos.color = Color.red;
				}

				Gizmos.DrawCube(node.position, Vector3.one * (nodeDiameter - distance));
			}
		}
	}

}
