﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int grid_y_Size = 64;
    public int grid_x_Size = 64;

    //public bool isWalking = true;
    //public bool isJumping = true;
    public bool isWall = false;
    public Vector3 position;

    public Node Parent;

    public int gCost;
    public int hCost;

    public int FCost { get { return gCost + hCost; } }

    public Node(bool m_isWall, Vector3 m_pos, int m_gridX, int m_gridY) {
        isWall = m_isWall;
        position = m_pos;
        grid_x_Size = m_gridX;
        grid_y_Size = m_gridY;
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
