﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astar : MonoBehaviour
{
    Grid grid;
    public Transform startPosition;
    public Transform targetPosition;
    // Start is called before the first frame update
    void Start()
    {
        grid = GetComponent<Grid>();
    }

    // Update is called once per frame
    void Update()
    {
        FindPath(startPosition.position, targetPosition.position);
    }

    void FindPath(Vector3 start_Pos, Vector3 target_Pos) {
        Node startNode = grid.NodeFromWorldPosition(start_Pos);
        Node targetNode = grid.NodeFromWorldPosition(target_Pos);

        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();

        openList.Add(startNode);

        while (openList.Count > 0) {
            Node currentNode = openList[0];
            for (int i = 1; i < openList.Count; i++) {
                if ((openList[i].FCost < currentNode.FCost || openList[i].FCost == currentNode.FCost) && (openList[i].hCost < currentNode.hCost || openList[i].hCost == currentNode.hCost) && (openList[i].gCost < currentNode.gCost || openList[i].gCost == currentNode.gCost)) {
                    currentNode = openList[i];
                }
            }
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (currentNode == targetNode) {
                GetFinalFath(startNode, targetNode);
            }

            foreach (Node neighborNode in grid.GetNeighboringNodes(currentNode)) {
                if (!neighborNode.isWall || closedList.Contains(neighborNode)) {
                    continue;
                }

                int moveCost = currentNode.gCost + GetDistance(currentNode, neighborNode);

                if (moveCost < neighborNode.gCost || !openList.Contains(neighborNode)) {
                    neighborNode.gCost = moveCost;
                    neighborNode.hCost = GetDistance(neighborNode, targetNode);
                    neighborNode.Parent = currentNode;

                    if (!openList.Contains(neighborNode)) {
                        openList.Add(neighborNode);
                    }
                }
            }
        }
    }

    void GetFinalFath(Node startNode, Node endNode) {
        List<Node> finalPath = new List<Node>();

        Node currentNode = endNode;

        while (currentNode != startNode) {
            finalPath.Add(currentNode);
            currentNode = currentNode.Parent;
        }

        finalPath.Reverse();

        grid.finalPath = finalPath;
    }

    int GetDistance(Node currentNode, Node neighborNode) {
        int ix = Mathf.Abs(currentNode.grid_x_Size - neighborNode.grid_x_Size);
        int iy = Mathf.Abs(currentNode.grid_y_Size - neighborNode.grid_y_Size);

        return ix + iy;
    }
}
