﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using PlayFab.CloudScriptModels;
using UnityEngine.SceneManagement;

public class PlayfabManager : MonoBehaviour
{

	#region 싱글톤
	private static PlayfabManager instance;

    public static PlayfabManager Instance {
        get { return instance; }
        set { instance = value; }
    }
    #endregion

    #region 일반 함수
    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        loginCanvas.gameObject.SetActive(true);
        registerCanvas.gameObject.SetActive(false);

        loginBtn.onClick.AddListener(() => Login());
        registerBtn.onClick.AddListener(() => OnRegisterClick());
        applyBtn.onClick.AddListener(() => OnApplyClick());

        registerPages.Add(0, registerCanvas);
        registerPages.Add(1, registerCanvas2);

        if (nextPageBtn != null)
        {
            nextPageBtn.onClick.AddListener(() => OnNextClick());
        }
    }
    #endregion

    #region 로그인 
    public InputField logemailInput;
    public InputField logpwdInput;
    public Text logprintMessage;

    public Canvas loginCanvas;
    public Button loginBtn;

    public Canvas registerCanvas;
    public Button registerBtn;

    public Canvas loginProgressCanvas;
    public Text loginProgress;

    public void Login() {
        var request = new LoginWithEmailAddressRequest{ Email = logemailInput.text, Password = logpwdInput.text};
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        var request = new GetAccountInfoRequest { PlayFabId = result.PlayFabId };
        PlayFabClientAPI.GetAccountInfo(request, OnAccountSuccess, OnAccountFailure);

        InventoryManager.Instance.TryInventoryInfo();
        PlayerManager.Instance.GetPlayerData();

        loginCanvas.gameObject.SetActive(false);
        loginProgressCanvas.gameObject.SetActive(true);

        InventoryManager.Instance.AddItem("Parts", "bottomPartsTest");
        InventoryManager.Instance.AddItem("Parts", "middlePartsTest");
        InventoryManager.Instance.AddItem("Parts", "weaponPartsTest");

        StartCoroutine("LoadNextScene");
    }

    IEnumerator LoadNextScene() {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Lobby");
        asyncOperation.allowSceneActivation = true;

        while (!asyncOperation.isDone)
        {
            loginProgress.text = "로딩 중 " + (asyncOperation.progress * 100) + "%";
            yield return null;
        }
    }

    private void OnLoginFailure(PlayFabError error)
    {
        logprintMessage.text = error.GenerateErrorReport();
    }

    private void OnAccountSuccess(GetAccountInfoResult result) {
        UIManager.Instance.nicknameUI.text = result.AccountInfo.TitleInfo.DisplayName;
    }

    private void OnAccountFailure(PlayFabError error)
    {
        Debug.Log("계정 정보 불러오기 실패 : ");
        Debug.Log(error.GenerateErrorReport());
    }
    #endregion

    #region 회원가입
    public InputField regemailInput;
    public InputField regpwdInput;
    public InputField regnameInput;

    public Text regprintMessage;

    public Toggle allCheck;
    public Toggle serviceCheck;
    public Toggle personalCheck;
    public Button nextPageBtn;
    public Button applyBtn;
    public Dropdown emailSelect;
    

    private Dictionary<int, Canvas> registerPages = new Dictionary<int, Canvas>();

    private int page = 0;

    public Canvas registerCanvas2;

    public void Register() {

        var request = new RegisterPlayFabUserRequest { Email = regemailInput.text + "@" + emailSelect.options[emailSelect.value].text, Password = regpwdInput.text, Username = regnameInput.text };
        PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnRegisterFailure);

        for (int i = 0; i < registerPages.Count; i++)
        {
            registerPages[i].gameObject.SetActive(false);
        }
        page = 0;
        loginCanvas.gameObject.SetActive(true);
    }

    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        Debug.Log("회원 가입 완료");
        var request = new UpdateUserTitleDisplayNameRequest { DisplayName = regnameInput.text };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdateSuccess, OnDisplayNameUpdateFailure);
    }

    private void OnRegisterFailure(PlayFabError error)
    {
        Debug.Log("회원 가입 실패");
        Debug.Log(error.GenerateErrorReport());
        regprintMessage.text = error.GenerateErrorReport();
    }

    private void OnDisplayNameUpdateSuccess(UpdateUserTitleDisplayNameResult result) {
        Debug.Log("닉네임 업뎃 성공");
    }

    private void OnDisplayNameUpdateFailure(PlayFabError error) {
        Debug.Log("닉네임 업뎃 실패 : ");
        Debug.Log(error.GenerateErrorReport());
    }

    public void OnRegisterClick()
    {
        loginCanvas.gameObject.SetActive(false);
        registerCanvas.gameObject.SetActive(true);
    }

    public void OnAllcheckClick()
    {
        serviceCheck.isOn = allCheck.isOn;
        personalCheck.isOn = allCheck.isOn;
    }

    public void OnNextClick()
    {
        if (!serviceCheck.isOn || !personalCheck.isOn) return;
        if (page + 1 >= registerPages.Count)
        {
            nextPageBtn.gameObject.SetActive(false);
            return;
        }


        registerPages[page].gameObject.SetActive(false);
        page += 1;

        registerPages[page].gameObject.SetActive(true);
    }

    public void OnApplyClick()
    {
        Register();
    }

    #endregion


}
