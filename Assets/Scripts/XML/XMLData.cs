﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class XMLData : MonoBehaviour
{
    private static XMLData instance;

    public static XMLData Instance {
        get {
            return instance;    
        }
    }

    private void Awake()
    {
        if (instance == null) {
            instance = this;
        }
    }

    public XmlNodeList LoadXMLData(string path, string name) {
        TextAsset txtAsset = (TextAsset)Resources.Load(path + "/" + name);

        XmlDocument xmlDoc = new XmlDocument();

        //Debug.Log("xml data Load : " + name);

        xmlDoc.LoadXml(txtAsset.text);

        XmlNodeList all_nodes = xmlDoc.SelectNodes("dataroot/" + name);

        return all_nodes;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
