﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace CAM {
    public class CameraController : MonoBehaviour
    {
        #region 변수
        public float cameraSpeed = 100f;
        public float zoomSensivity = 100f;
        public float borderThickness = 10f;

        private float mouseX;
        private float mouseY;
        private float zoomY = 20f;

        GameObject cameraObj;
        Camera camera;
        GameObject player;

        Vector3 camPos;
        #endregion

        private void Awake()
        {
            if (cameraObj == null)
            {
                cameraObj = this.gameObject;
                camera = cameraObj.GetComponent<Camera>();

                camPos = transform.position;

                player = GameObject.Find("Player");
            }
        }

        private void LateUpdate()
        {
            mouseX = Input.mousePosition.x;
            mouseY = Input.mousePosition.y;
        }

        void Update()
        {
            bool rotating = CameraRotate();

            if (!rotating && !player.GetComponent<Unit.SelectUnit>().isSelecting)
            {
                CameraMove();
            }



            transform.position = new Vector3(camPos.x, (transform.forward * -zoomY).y, camPos.z);
        }

        #region 카메라 조작 
        void CameraMove()
        {
            float mx, my, mz, sx, sy;

            mx = Input.GetAxis("Horizontal");
            my = Input.GetAxis("Mouse ScrollWheel");
            mz = Input.GetAxis("Vertical");

            Vector3 pos = transform.position;


            if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - borderThickness)
            {
                Debug.Log("forward");
                pos.z += cameraSpeed * Time.deltaTime;

            }
            else if (Input.GetKey("s") || Input.mousePosition.y <= borderThickness)
            {
                pos.z -= cameraSpeed * Time.deltaTime;
            }

            if (Input.GetKey("a") || Input.mousePosition.x >= Screen.width - borderThickness)
            {
                pos.x += cameraSpeed * Time.deltaTime;
            }
            else if (Input.GetKey("d") || Input.mousePosition.x <= borderThickness)
            {
                pos.x -= cameraSpeed * Time.deltaTime;
            }

            zoomY = Mathf.Lerp(zoomY, zoomY - my * zoomSensivity, Time.deltaTime);
            zoomY = Mathf.Clamp(zoomY, 5f, 25f);

            camPos = pos;
        }

        bool CameraRotate()
        {
            float easeFactor = 10f;
            Vector3 ang = transform.localEulerAngles;

            if (Input.GetKey(KeyCode.LeftAlt))
            {
                if (Input.mousePosition.x != mouseX)
                {
                    float cameraRotationY = (Input.mousePosition.x - mouseX) * easeFactor * Time.deltaTime;
                    ang.y += cameraRotationY;
                }

                if (Input.mousePosition.y != mouseY)
                {
                    float cameraRotationX = (Input.mousePosition.y - mouseY) * easeFactor * Time.deltaTime;
                    ang.x -= cameraRotationX;
                }

                transform.localEulerAngles = ang;

                return true;
            }

            return false;
        }
        #endregion
    }

}
