﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class PlayerManager : MonoBehaviour
{
	#region 싱글톤
	private static PlayerManager instance;

	public static PlayerManager Instance {
		get { return instance; }
		set { instance = value; }
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else {
			Destroy(this.gameObject);
		}
	}
	#endregion

	#region 레벨
	public void GetPlayerData() {
		PlayFabClientAPI.GetUserPublisherData(new GetUserDataRequest(), onSuccess, onError);
	}

	private void onSuccess(GetUserDataResult result) {
		Debug.Log("Level : " + result.Data["exp"].Value);
	}

	private void onError(PlayFabError error) { 
	
	}
	#endregion
}
