﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Player : MonoBehaviour
{
    #region 플레이어 정보
    private string nickname;
	#endregion

	#region 유닛 변수
	public List<Unit.Unit> units;
    public LayerMask layerMask;
    public Transform targetPosition;
	#endregion

	#region 팀 변수
	public int team;
	#endregion

	// Start is called before the first frame update
	void Start()
    {
        //BottomPartTest test = new BottomPartTest();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1)) {
            Vector3 pos = Input.mousePosition;
            pos.z = Camera.main.farClipPlane;

            Vector3 dir = Camera.main.ScreenToWorldPoint(pos);

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, dir, out hit, pos.z, layerMask))
            {
                targetPosition.position = hit.point;

                foreach (Unit.Unit selectUnit in Unit.SelectUnit.Instance.selectUnits)
                {
                    selectUnit.GetComponent<Pathfinding.AIDestinationSetter>().target = targetPosition;
                }
            }
        }
    }

    #region 유닛 생산 및 파괴 
    public void SpawnUnit(GameObject unit, Vector3 m_pos)
    {

    }
    #endregion
}
