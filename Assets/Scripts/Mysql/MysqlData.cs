﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MysqlData : MonoBehaviour
{
	public string ip = "http://119.70.74.145/";

	private void Awake()
	{
		CallLoadMysql();
	}

	public void CallLoadMysql() {
		StartCoroutine("LoadMysqlData");
	}

	IEnumerator LoadMysqlData() {
		WWW www = new WWW(ip);
		yield return www;

		if (www.text == "0")
		{
			Debug.Log("Successfully Connect on Mysql");
		}
		else {
			Debug.Log("Connection failed");
		}
	}
}
